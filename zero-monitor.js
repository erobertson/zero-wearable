#!/usr/bin node
console.log('start');

var http = require('http');
var gpio = require('pi-gpio');
var oledLib = require('oled-spi');
var font = require('oled-font-5x7');
var currentIp = null;
var lineSize = 8;
var oledOpts = {
	width: 128,
	height: 64,
	dcPin: 23,
	rstPin: 24
};

var sensorLib = require('node-dht-sensor');
//refactor
var readingC;
var readingF;
var humidity;

var sensor = {
	initialize: function() {
		return sensorLib.initialize(22, 4);
	},
	read: function(){
		var reading = sensorLib.read();
		readingC = reading.temperature.toFixed(2);
		readingF = (readingC * (9.0 / 5.0) + 32).toFixed(2);
		humidity = reading.humidity.toFixed(2)
		setTimeout(function() {
			sensor.read();
		}, 2500);
	}
};

var oled = new oledLib(oledOpts);
oled.begin(function(){
	console.log('oled ready.');
	oled.turnOnDisplay();
	oled.setCursor(10,lineSize * 2);
	oled.drawLine(1, 1, 127, 1, 1);
	oled.drawLine(1, 1, 1, 63, 1);
	oled.drawLine(1, 63, 127, 63, 1);
	oled.drawLine(127, 1, 127, 63, 1);
	oled.writeString(font, 1, 'Program start.', 1, true);
});


function printTemperatureToDisplay(){
	var messageLine0 = readingC + ' C ' + readingF + ' F';
	var messageLine1 = 'Humidity: ' + humidity + '%';
	oled.clearDisplay();
	oled.setCursor(0, 0);
	oled.writeString(font, 1, messageLine0, 1, false);
	oled.setCursor(0, lineSize);
	oled.writeString(font, 1, messageLine1, 1, false);
}

process.on('SIGINT', function(){
	oled.clearDisplay();
	oled.turnOffDisplay();
	process.exit();
});

function printIp(){
	//get list of network interfaces
	var ip = "No IP";
	try{
		var osInt = require('os').networkInterfaces();
        	ip = osInt.wlan0[0].address;
		currentIp = ip;
	}catch (e){
		currentIp = null;
		//error is reported as no IP on the LCD
	}
        oled.setCursor(0, lineSize * 3);
        oled.writeString(font, 1, ip, 1, true);
}

function printTime(){
	oled.setCursor(0, lineSize * 4);
	oled.writeString(font, 1, new Date().toString().substring(16,24), 1, true);
}

function logTemperature(readingC, readingF, humidity){
	//eventually the plan is to store these values and log them
	//when network becomes available but for now just only log 
	//when network is available
	if(!currentIp) return;
	var httpOptions = {
		host: "whispering-brook-51660.herokuapp.com",
		path: "/logService/?deviceName=piZero&reportC="
			+readingC+"&reportF="+readingF+
			"&status=Pi%20Check-In:"+readingC+"%20C%20"+
			readingF+"%20F%20"+humidity+"%20percent%20humidity."
	};

	callback = function(response){
		var resp = '';
	
		response.on('data', function(chunk){
			resp += chunk;
		});

		response.on('end', function(){
			console.log('Response complete:');
			console.log(resp);
		});
	}
	
	http.request(httpOptions, callback).end();
}

setInterval(function(){
		//logTemperature(readingC, readingF, humidity);
	    //}, 1000 * 60 * 15);
	oled.begin(function(){
		printTemperatureToDisplay();
		printIp();
		printTime();
	});
	//temp
	console.log('ReadingC: ' + readingC + ' ReadingF: ' + readingF + ' Humidity: ' + humidity);
	}, 5000);

if(sensor.initialize()){
	sensor.read();
}else{
	console.warn('Failed to initialize dht22.');
}
